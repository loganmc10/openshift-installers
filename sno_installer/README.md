# Steps to install SNO
## Prerequisites
coreos-installer:
```
sudo dnf -y install coreos-installer
```

## Create install-config.yaml
```
cp sample-install-config.yaml install-config.yaml
```
You must enter:
- Domain name
- Cluster name
- Pull secret
- SSH key

Make any other edits as needed

### Disconnected Install
If you're doing a disconnected install, you'll need to add the following to install-config.yaml:
```
imageContentSources: 
  - mirrors:
      - <mirror_registry_url>/openshift/release
    source: quay.io/openshift-release-dev/ocp-release
  - mirrors:
      - <mirror_registry_url>/openshift/release
    source: quay.io/openshift-release-dev/ocp-v4.0-art-dev
  - mirrors:
      - <mirror_registry_url>/operator-framework
    source: quay.io/operator-framework
  - mirrors:
      - <mirror_registry_url>/openshift4
    source: registry.redhat.io/openshift4
  - mirrors:
      - <mirror_registry_url>/redhat
    source: registry.redhat.io/redhat
additionalTrustBundle: |
  -----BEGIN CERTIFICATE-----
  ...
  -----END CERTIFICATE-----
```
The additionalTrustBundle can be gathered from <quay_install>/quay-config/ssl.cert

After the install is complete, remove the default OLM catalog sources:
```
oc patch OperatorHub cluster --type json -p '[{"op": "add", "path": "/spec/disableAllDefaultSources", "value": true}]'
```
Apply the following manifest to enable the mirror registry as a catalog source:
```
apiVersion: operators.coreos.com/v1alpha1
kind: CatalogSource
metadata:
  name: redhat-operator-index
  namespace: openshift-marketplace
spec:
  image: <mirror_registry_url>/redhat/redhat-operator-index:v4.11
  sourceType: grpc
```
### install-config.yaml parameters
```
# Get the documentation of the resource and its fields
openshift-install explain installconfig

# Get the documentation of a AWS platform
openshift-install explain installconfig.platform.aws
```

## Generate installer ISO
```
ansible-playbook installer-playbook.yaml
```
It will prompt you for the version of OCP you'd like to install, for example:
- 4.8.29
- 4.9.25
- stable-4.11

The playbook will create an 'output' folder, which contains:
- The install ISO
- An 'auth' folder with the kubeconfig
