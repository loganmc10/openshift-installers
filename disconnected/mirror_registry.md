# Create a mirror registry (Quay)
## Download
Download from https://console.redhat.com/openshift/downloads#tool-mirror-registry
## Install
quayHostname will default to the machine hostname, and quayRoot defaults to ```/etc/quay-install```
```
sudo ./mirror-registry install --quayHostname <host_example_com> --quayRoot <install_directory_name>
```
The password will be printed when the install is complete.
## Get mirror registry pull secret
```
podman login --authfile mirror-secret.json -u init -p <password_from_above> <mirror_registry>:8443 --tls-verify=false 
```
This will generate a pull-secret.txt file with the credentials for the registry. You'll need this file later when you go to install OpenShift via the Assisted Service.
## Removing the mirror registry (if needed)
```
sudo ./mirror-registry uninstall -v --quayRoot <install_directory_name>
```
# Populate the mirror registry
## Download oc mirror plugin
Download from https://console.redhat.com/openshift/downloads#tool-oc-mirror-plugin

Put into PATH somewhere and make executable
## Get Red Hat pull secret
Download from https://console.redhat.com/openshift/downloads#tool-pull-secret

Make a copy in JSON format:
```
cat ./pull-secret.txt | jq . > rh-secret.json
```
## Merge pull secrets together and install
```
mkdir -p ~/.docker
jq -s '.[0] * .[1]' mirror-secret.json rh-secret.json > ~/.docker/config.json
```
## Create and enter working directory
```
mkdir mirror
cd mirror
```
## Create ImageSetConfiguration
Create a ```imageset-config.yaml``` file as follows inside the working directory:
```
apiVersion: mirror.openshift.io/v1alpha2
kind: ImageSetConfiguration
archiveSize: 4
mirror:
  platform:
    channels:
      - name: stable-4.12
        minVersion: 4.12.0
        type: ocp
    graph: false
  operators:
    - catalog: registry.redhat.io/redhat/redhat-operator-index:v4.12
      packages:
        - name: metallb-operator
          channels:
            - name: stable
        - name: cluster-logging
          channels:
            - name: stable
        - name: ptp-operator
          channels:
            - name: stable
        - name: sriov-network-operator
          channels:
            - name: stable
        - name: local-storage-operator
          channels:
            - name: stable
        - name: mcg-operator
          channels:
            - name: stable-4.12
        - name: ocs-operator
          channels:
            - name: stable-4.12
        - name: odf-csi-addons-operator
          channels:
            - name: stable-4.12
        - name: odf-operator
          channels:
            - name: stable-4.12
        - name: odf-lvm-operator
          channels:
            - name: stable-4.12
        - name: advanced-cluster-management
          channels:
            - name: release-2.6
        - name: multicluster-engine
          channels:
            - name: stable-2.1
        - name: topology-aware-lifecycle-manager
          channels:
            - name: stable
        - name: bare-metal-event-relay
          channels:
            - name: stable
        - name: openshift-gitops-operator
          channels:
            - name: latest
        - name: kubernetes-nmstate-operator
          channels:
            - name: stable
  additionalImages:
    - name: registry.redhat.io/openshift4/ztp-site-generate-rhel8:v4.12
storageConfig:
  registry:
    imageURL: <mirror_registry>:8443/mirror/oc-mirror-metadata
    skipTLS: true
```
## Mirror contents into registry
```
oc mirror --config=./imageset-config.yaml docker://<mirror_registry>:8443 --dest-skip-tls
```
## Updating mirror registry
To update the mirror registry, simply re-run the oc mirror command above.
# Configuring cluster to use mirror registry
## Update Image config
```
oc create configmap mirror-registry --from-file=<mirror_registry>..8443=<install_directory_name>/quay-config/ssl.cert -n openshift-config
oc patch image.config.openshift.io/cluster --patch '{"spec":{"additionalTrustedCA":{"name":"mirror-registry"}}}' --type=merge
```
## Update pull secret
```
oc set data secret/pull-secret -n openshift-config --from-file=.dockerconfigjson=./mirror-secret.json
```
## Update cluster config
Once the oc mirror command is complete, there will be a "results" folder in the working directory under the ```oc-mirror-workspace``` folder.

The results folder has 2 manifests that need to be applied (ImageContentSourcePolicy and CatalogSource). Applying these manifests will allow the cluster to use the mirror registry.

The CatalogSource name is 'redhat-operator-index' by default. You might want to rename that to 'redhat-operators' before applying it, since 'redhat-operators' is the name of the default source and some things expect that name.
```
oc patch OperatorHub cluster --type json -p '[{"op": "add", "path": "/spec/disableAllDefaultSources", "value": true}]'
oc apply -f ./oc-mirror-workspace/results-<result_number>/
```
