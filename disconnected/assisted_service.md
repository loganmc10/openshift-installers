# Install Assisted Service
See https://github.com/openshift/assisted-service/tree/master/deploy/podman
## Make working directory
```
mkdir assisted
cd assisted
```
## Podman deployment prep
```
wget https://raw.githubusercontent.com/openshift/assisted-service/master/deploy/podman/pod.yml
wget https://raw.githubusercontent.com/openshift/assisted-service/master/deploy/podman/configmap.yml
```
Change ```IMAGE_SERVICE_BASE_URL``` and ```SERVICE_BASE_URL``` in configmap.yml to match the hostname or IP address of your host. For example if your IP address is 192.168.122.2, then the ```SERVICE_BASE_URL``` would be set to http://192.168.122.2:8090. 

Update ```OS_IMAGES``` to point to an ISO that is accessible by the Assisted Service.

Update ```RELEASE_IMAGES``` to point to your mirror registry. For instance: ```<host_example_com>:8443/openshift/release:4.11.4-x86_64```
## Run Assisted Service
```
podman play kube --configmap configmap.yml pod.yml
```
## Install OpenShift
The UI will available at: ```http://<host-ip-address>:8080```

Your pull secret should be the pull-secret.txt that was generated when you set up the mirror registry. See [get mirror registry pull secret](mirror_registry.md#get-mirror-registry-pull-secret)
## Update cluster to use mirror registry after installation
See [configuring cluster to use mirror registry](mirror_registry.md#configuring-cluster-to-use-mirror-registry)
## Remove Assisted Service (if needed)
```
podman play kube --down pod.yml
```
